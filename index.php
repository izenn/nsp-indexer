<?php

/**
 * PHP Indexer for Switch NSP XCI NSZ and XCZ files
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

require 'lib/Utils.php';
require 'config.default.php';

define('REGEX_TITLEID', '0100[0-9A-F]{12}');
define('REGEX_TITLEID_BASE', '0100[0-9A-F]{8}[02468ACE]000');
define('REGEX_TITLEID_UPDATE', '0100[0-9A-F]{9}800');

define("CACHE_DIR", './cache');
define("TMP_UPLOAD_DIR", './cache/tmp_upload');
if (!file_exists(CACHE_DIR)) {
    if (!@mkdir(CACHE_DIR)) {
        echo "Could not create the cache directory '" . CACHE_DIR . "', please make sure your webserver has write permission to the local directory.";
        die();
    }
}
if (!file_exists(TMP_UPLOAD_DIR)) {
    mkdir(TMP_UPLOAD_DIR);
}

if (!function_exists('curl_version')) {
    echo "curl extension isn't installed please install it and refresh page";
    die();
}

if (file_exists('config.php')) {
    include 'config.php';
}

if (isset($_GET["tinfoil"])) {
    header("Content-Type: application/json");
    header('Content-Disposition: filename="main.json"');
    echo outputTinfoil();
    die();
} elseif (isset($_GET["DBI"])) {
    header("Content-Type: text/plain");
    echo outputDbi();
    die();
}

$enableDecryption = false;
$zstdSupport = false;
$fileupload = false;

if (is_writable($gameDir)) {
    $fileupload = true;
}

global $keyFile;
if (!empty($keyFile) && file_exists($keyFile)) {
    $keyList = parse_ini_file($keyFile);
    if (count($keyList) > 0 && !is32bit()) {
        $enableDecryption = true;
    }
}

if (!extension_loaded('openssl') && $enableDecryption == true) {
    echo "openssl insn't installed please install it and refresh page";
    die();
}

if (extension_loaded('yazstd') && $enableDecryption == true) {
    $zstdSupport = true;
}

if (!function_exists('gmp_add') && $enableDecryption == true) {
    echo "php gmp is not installed please install it and refresh page";
    die();
}

$version = trim(file_get_contents('./VERSION'));

 /**
  * Returns info from the specified ROM
  *
  * @param string $path the relative path of the rom
  *
  * @return array json array of rom info
  */
function outputRomInfo($path)
{
    global $gameDir;
    if ($romInfo = romInfo($gameDir . DIRECTORY_SEPARATOR . $path)) {
        return json_encode($romInfo);
    } else {
        return json_encode(array('int' => -1));
    }
}

/**
 * Returns a list of the files in a specified direcotry
 *
 * @param string $path path to get list of files
 * @param bool   $skip inlcude skip extension
 *
 * @return array
 */
function getFileList($path, $skip = false)
{
    global $allowedExtensions, $skipExtension;
    $fileListAllowedExtensions = $allowedExtensions;
    if ($skip) {
        $fileListAllowedExtensions = array_merge($allowedExtensions, ["$skipExtension"]);
    }

    $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST);
    $arrFiles = array();
    foreach ($files as $file) {
        $parts = explode('.', $file);
        if (!$file->isDir() && in_array(strtolower(array_pop($parts)), $fileListAllowedExtensions)) {
            if (strtolower(PHP_SHLIB_SUFFIX) === 'dll') {
                array_push($arrFiles, str_replace("\\", "\\\\", (str_replace($path . DIRECTORY_SEPARATOR, '', $file->getPathname()))));
            } else {
                array_push($arrFiles, str_replace($path . DIRECTORY_SEPARATOR, '', $file->getPathname()));
            }
        }
    }
    natcasesort($arrFiles);
    return array_values($arrFiles);
}

 /**
  * Returns if the title is a base title, an update or a dlc
  *
  * @param string $titleId title id of the file
  *
  * @return string
  */
function getTitleIdType($titleId)
{
    if (preg_match('/' . REGEX_TITLEID_BASE . '/i', $titleId) === 1) {
        return 'base';
    } elseif (preg_match('/' . REGEX_TITLEID_UPDATE . '/i', $titleId) === 1) {
        return 'update';
    } elseif (preg_match('/' . REGEX_TITLEID . '/i', $titleId) === 1) {
        return 'dlc';
    }
    return false;
}

 /**
  * Returns the base title ID of an update or dlc
  *
  * @param string $titleId the id of the update or dlc
  *
  * @return string the calculated base title id
  */
function getBaseTitleId($titleId)
{
    if (getTitleIdType($titleId) == 'update') {
        return substr_replace($titleId, "000", -3);
    } elseif (getTitleIdType($titleId) == 'dlc') {
        return dlcIdToBaseId($titleId);
    }
    return strtoupper($titleId);
}

/**
 * Returns the base title id of a dlc file
 *
 * @param string $titleId the id of the dlc
 *
 * @return string the calculated base title id
 */
function dlcIdToBaseId($titleId)
{
        // find the Base TitleId (TitleId of the Base game with the fourth bit shifted by 1)
    $dlcBaseId = substr_replace($titleId, "000", -3);
    $offsetBit = hexdec(substr($dlcBaseId, 12, 1));
    $baseTitleBit = strtoupper(dechex($offsetBit - 1));
    return substr_replace($dlcBaseId, $baseTitleBit, -4, 1);
}

/**
 * Goes through files, creating two arrays,
 * one with the title ids and all their associated files
 * the other with the unmatched title ids
 *
 * @param array $files array of files to iterate through
 *
 * @return array multidimensional array of matched and unmatched title ids
 */
function matchTitleIds($files)
{
    $titles = [];
    // first round, get all Base TitleIds
    foreach ($files as $key => $file) {
        // check if we have a Base TitleId (0100XXXXXXXXY000, with Y being an even number)
        if (preg_match('/(?<=\[)' . REGEX_TITLEID_BASE . '(?=])/', strtoupper($file), $titleIdMatches) === 1) {
            $titleId = strtoupper($titleIdMatches[0]);
            $titles[$titleId] = array(
                "path" => $file,
                "updates" => array(),
                "dlc" => array()
            );
            unset($files[$key]);
        }
    }

    $unmatched = [];
    // second round, match Updates and DLC to Base TitleIds
    foreach ($files as $key => $file) {
        if (preg_match('/(?<=\[)' . REGEX_TITLEID . '(?=])/', strtoupper($file), $titleIdMatches) === 0) {
                        // file does not have any kind of TitleId, skip further checks
            array_push($unmatched, $file);
            continue;
        }
        $titleId = strtoupper($titleIdMatches[0]);

        // find Updates (0100XXXXXXXXX800)
        if (preg_match('/^' . REGEX_TITLEID_UPDATE . '$/i', $titleId) === 1) {
            $titleId = strtoupper($titleId);
            if (preg_match('/(?<=\[v).+?(?=])/i', $file, $versionMatches) === 1) {
                $version = strtoupper($versionMatches[0]);
                $baseTitleId = getBaseTitleId($titleId);
                                // add Update only if the Base TitleId for it exists
                if (array_key_exists($baseTitleId, $titles)) {
                    if ($titles[$baseTitleId]) {
                        $titles[$baseTitleId]['updates'][$version] = array(
                            "path" => $file
                        );
                    }
                }
            }
        } else {
            $baseTitleId = getBaseTitleId($titleId);
            // add DLC only if the Base TitleId for it exists
            if (array_key_exists($baseTitleId, $titles)) {
                if ($titles[$baseTitleId]) {
                    $titles[$baseTitleId]['dlc'][$titleId] = array(
                        "path" => $file
                    );
                }
            }
        }
    }
    return array(
        'titles' => $titles,
        'unmatched' => $unmatched
    );
}

/**
 * Takes titles.json generated by nut and condenses it to only the information used by nsp-indexer
 *
 * @param array $jsonData original titles.json
 *
 * @return array condensed titles.json
 */
function rewriteTitlesJson($jsonData)
{
    $parsedjsonData = json_decode($jsonData);
    $retjson = array();
    foreach ($parsedjsonData as $key => $val) {
        $tmpentry = new \stdClass();
        $tmpentry->id = strtoupper($val->id);
        $tmpentry->name = $val->name;
        $tmpentry->iconUrl = $val->iconUrl;
        $tmpentry->bannerUrl = $val->bannerUrl;
        $tmpentry->intro = $val->intro;
        $tmpentry->size = $val->size;
        $tmpentry->version = $val->version;
        $tmpentry->releaseDate = $val->releaseDate;
        $tmpentry->screenshots = $val->screenshots;
        $retjson[strtoupper($val->id)] = $tmpentry;
    }
    return $retjson;
}

/**
 * Takes versions.json generated by nut and condenses it to only the information used by nsp-indexer
 *
 * @param array $jsonData original versions.json
 *
 * @return array condensed versions.json
 */
function rewriteVersionsJson($jsonData)
{
    $parsedjsonData = json_decode($jsonData);
    $retjson = array();
    foreach ($parsedjsonData as $key => $val) {
        $retjson[strtoupper($key)] = $val;
    }
    return $retjson;
}

/**
 * Gets the titles.json and versions.json for nspindexer.js regenerating the cache files if necessary
 *
 * @param string $type    titles or versions
 * @param bool   $refresh regenerate the json cache
 *
 * @return array json array of requested type
 */
function getMetadata($type, $refresh = false)
{
    if (!$refresh && file_exists(CACHE_DIR . "/" . $type . ".json")) {
        $json = file_get_contents(CACHE_DIR . "/" . $type . ".json");
    } else {
        global $titleDbFolder;
        $json = "";
        if ($type != "versions") {
            $json = file_get_contents($titleDbFolder . "/titles.json");
            $retjson = json_encode(rewriteTitlesJson($json), JSON_PRETTY_PRINT);
            file_put_contents(CACHE_DIR . DIRECTORY_SEPARATOR . $type . ".json", $retjson);
            return json_decode($retjson, true);
        } else {
            $json = strtoupper(file_get_contents($titleDbFolder . "/versions.json"));
            $retjson = json_encode(rewriteVersionsJson($json), JSON_PRETTY_PRINT);
            file_put_contents(CACHE_DIR . DIRECTORY_SEPARATOR . $type . ".json", $retjson);
            return json_decode($retjson, true);
        }
    }
    return json_decode($json, true);
}

/**
 * Regenerates the cache for titles.json and versions.json
 *
 * @return array refresh status
 */
function refreshMetadata()
{
    $refreshed = array();

    foreach (array("versions", "titles") as $type) {
        if (!file_exists(CACHE_DIR . DIRECTORY_SEPARATOR . $type . ".json") || filemtime(CACHE_DIR . DIRECTORY_SEPARATOR . $type . ".json") < (time() - 60 * 5)) {
            getMetadata($type, true);
            array_push($refreshed, $type);
        }
    }
    return json_encode(
        array(
            "int" => count($refreshed),
            "msg" => "Metadata " . ((count($refreshed) > 0) ? "updated: " . join(", ", $refreshed) : "not updated")
        )
    );
}

/**
 * Sends the active config to nspindexer.js
 *
 * @return array json array of configs used by javascript
 */
function outputConfig()
{
    global $contentUrl, $version, $enableNetInstall, $switchIp, $enableDecryption, $enableRename, $zstdSupport, $fileupload, $showWarnings, $skipExtension;
    return json_encode(
        array(
            "contentUrl" => $contentUrl,
            "version" => $version,
            "enableNetInstall" => $enableNetInstall,
            "enableRename" => $enableRename,
            "enableRomInfo" => $enableDecryption,
            "zstdSupport" => $zstdSupport,
            "fileupload" => $fileupload,
            "showWarnings" => $showWarnings,
            "switchIp" => $switchIp,
            "skipExtension" => $skipExtension
        )
    );
}

/**
 * Creates json used to create the title cards
 *
 * @param bool $forceUpdate regenerate games.json
 *
 * @return array json array of title information
 */
function outputTitles($forceUpdate = false)
{
    if (!$forceUpdate && file_exists(CACHE_DIR . "/games.json") && (filemtime(CACHE_DIR . "/games.json") > (time() - 60 * 5))) {
        $json = file_get_contents(CACHE_DIR . "/games.json");
    } else {
        global $gameDir;
        $versionsJson = getMetadata("versions");
        $titlesJson = getMetadata("titles");
        $titles = matchTitleIds(getFileList($gameDir, true));
        $output = array();
        foreach ($titles['titles'] as $titleId => $title) {
            $latestVersion = 0;
            if (array_key_exists(strtoupper($titleId), $versionsJson)) {
                $latestVersion = array_key_last($versionsJson[strtoupper($titleId)]);
                $latestVersionDate = array_slice($versionsJson[strtoupper($titleId)], -1);
            } else {
                $releaseDate = \DateTime::createFromFormat('Ymd', sprintf("%s", $titlesJson[strtoupper($titleId)]["releaseDate"]));
                $latestVersionDate = $releaseDate->format('Y-m-d');
            };

            $game = array(
                "path" => $title["path"],
                "fileType" => guessFileType($gameDir . DIRECTORY_SEPARATOR . $title["path"]),
                "name" => $titlesJson[$titleId]["name"] ?: $title["path"],
                "thumb" => $titlesJson[$titleId]["iconUrl"] ?: "/img/questionmark.png",
                "banner" => $titlesJson[strtoupper($titleId)]["bannerUrl"],
                "intro" => $titlesJson[strtoupper($titleId)]["intro"],
                "latest_version" => $latestVersion,
                "latest_date" => $latestVersionDate,
                "size" => $titlesJson[strtoupper($titleId)]["size"],
                "size_real" => getFileSize($gameDir . DIRECTORY_SEPARATOR . $title["path"]),
                "screenshots" => $titlesJson[strtoupper($titleId)]["screenshots"]
            );
            $updates = array();
            foreach ($title["updates"] as $updateVersion => $update) {
                if (!array_key_exists($titleId, $versionsJson)) {
                    $updateDate = "unknown";
                } else if (!array_key_exists($updateVersion, $versionsJson[$titleId])) {
                    $updateDate = "unknown";
                } else {
                    $updates[(int)$updateVersion] = array(
                        "path" => $update["path"],
                        "date" => $latestVersionDate = $versionsJson[strtoupper($titleId)][$updateVersion],
                        "size_real" => getFileSize($gameDir . DIRECTORY_SEPARATOR . $update["path"])
                    );
                }
            }

            if (!array_key_exists($latestVersion, $updates) && $latestVersion != 0) {
                $updates[(int)$latestVersion] = array(
                "path" => "missing",
                "date" => $latestVersionDate,
                "size_real" => "missing"
                );
            }

            $game['updates'] = $updates;

            if ($game["fileType"] == "XCI" && preg_match('/[\[]v[1-9]/', $game["path"])) {
                $updatedXCI = romInfo($gameDir . DIRECTORY_SEPARATOR . $game['path']);
                $xciTitles = $updatedXCI->titleInfo;
                unset($xciTitles[strtolower($titleId)]);
                unset($xciTitles[strtolower($updateTitleId)]);
                $dlcTitles = array_keys($xciTitles);
                foreach ($dlcTitles as $dlc) {
                    $title["dlc"][$dlc]["path"] = $game["path"];
                }
            }

            $baseTitleId = substr($titleId, 0, -3);
            $offsetBit = hexdec(substr($baseTitleId, -1, 1));
            $baseDlcBit = strtoupper(dechex($offsetBit + 1));
            $baseDlcId = substr_replace($baseTitleId, $baseDlcBit, -1, 1);
            $dlcAll = preg_grep("/^$baseDlcId/", array_keys($titlesJson));

            $dlcs = array();
            foreach ($title["dlc"] as $dlcId => $d) {
                $key = array_search(strtoupper($dlcId), $dlcAll);
                if ($key !== false) {
                    unset($dlcAll[$key]);
                }
                $dlcs[strtoupper($dlcId)] = array(
                    "path" => $d["path"],
                    "name" => $titlesJson[strtoupper($dlcId)]["name"],
                    "size" => $titlesJson[strtoupper($dlcId)]["size"],
                    "size_real" => getFileSize($gameDir . DIRECTORY_SEPARATOR . $d["path"])
                );
            }

            foreach ($dlcAll as $dlcMissing) {
                $dlcs[strtoupper($dlcMissing)] = array(
                    "path" => "missing",
                    "name" => $titlesJson[strtoupper($dlcMissing)]["name"],
                    "latest_version" => $latestVersion,
                    "size" => "missing",
                    "size_real" => "missing"
                );
            }

            ksort($dlcs);
            $game['dlc'] = $dlcs;
            $output[strtoupper($titleId)] = $game;
        }
        $json = json_encode(
            array(
                'titles' => $output,
                'unmatched' => $titles['unmatched']
            )
        );
        file_put_contents(CACHE_DIR . "/games.json", $json);
    }
    return $json;
}

/**
 * Creates output for tinfoil
 *
 * @return array json array used for tinfoil output
 */
function outputTinfoil()
{
    global $gameDir, $contentUrl, $extPort;
    $output = array();
    $fileList = getFileList($gameDir);
    asort($fileList);
    $output["total"] = count($fileList);
    $output["files"] = array();
    $urlSchema = getURLSchema();
    foreach ($fileList as $file) {
        if (strtolower(PHP_SHLIB_SUFFIX) === 'dll') {
            $file = str_replace("\\\\", "/", $file);
        }

        if ($extPort) {
            if (!is32bit()) {
                $output["files"][] = [
                    'url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . ":" . $extPort . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => getFileSize($gameDir . DIRECTORY_SEPARATOR . $file)
                ];
            } else {
                $output["files"][] = [
                    'url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . ":" . $extPort . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => floatval(getFileSize($gameDir . DIRECTORY_SEPARATOR . $file))
                ];
            }
        } else {
            if (!is32bit()) {
                $output["files"][] = [
                    'url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => getFileSize($gameDir . DIRECTORY_SEPARATOR . $file)
                ];
            } else {
                $output["files"][] = [
                    'url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => floatval(getFileSize($gameDir . DIRECTORY_SEPARATOR . $file))
                ];
            }
        }
    }
    return json_encode($output);
}

/**
 * Creates output for dbi
 *
 * @return array json array used for DBI output
 */
function outputDbi()
{
    global $contentUrl, $gameDir, $extPort;
    $urlSchema = getURLSchema();
    $fileList = getFileList($gameDir);
    $output = "";
    foreach ($fileList as $file) {
        if (strtolower(PHP_SHLIB_SUFFIX) === 'dll') {
            $file = str_replace("\\\\", "/", $file);
        }

        if ($extPort) {
            $output .= $urlSchema . '://' . $_SERVER['SERVER_NAME'] . ":" . $extPort . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))) . "\n";
        } else {
            $output .= $urlSchema . '://' . $_SERVER['SERVER_NAME'] . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))) . "\n";
        }
    }
    return $output;
}

/**
 * Generates output for internal rom files
 *
 * @param string $romfilecontents relative path of file to get info from
 * @param string $romfile         filename of rom to scan
 *
 * @return array json array with content results
 */
function outputRomFileContents($romfilecontents, $romfile)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $romfilecontents;
    $ret = romFileListContents($path, $romfile);
    if ($ret) {
        return json_encode(
            array(
                "int" => 0,
                "ret" => $ret,
                "path" => $romfilecontents,
                "ncaName" => $romfile
            )
        );
    } else {
        return json_encode(
            array(
                'int' => -1, 'msg' => "Unable to open selected file"
            )
        );
    }
}

/**
 * Analyzes nca files inside a rom
 *
 * @param string $ncafileanalyze relative path to rom
 * @param string $romfile        filename of rom to get info from
 *
 * @return array json array used by tinfoil
 */
function outputncafileAnalyze($ncafileanalyze, $romfile)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $ncafileanalyze;
    $ret = ncaFileAnalyze($path, $romfile);
    if ($ret) {
        return json_encode(
            array(
                "int" => 0,
                "ret" => $ret,
                "path" => $ncafileanalyze,
                "ncaName" => $romfile
            )
        );
    } else {
        return json_encode(
            array(
                'int' => -1, 'msg' => "Unable to open selected file"
            )
        );
    }
}

/**
 * Outputs initial rom file information
 *
 * @param string $romfilename relative path to rom
 * @param string $romfile     filename of rom to get info from
 *
 * @return mixed octet-stream of results
 */
function outputRomFile($romfilename, $romfile)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $romfilename;
    romFile($path, $romfile);
}

/**
 * Firmware update info for xci files
 *
 * @param string $xcifile    relative path to rom
 * @param string $fwfilename filename of rom to get info from
 *
 * @return mixed octet-stream of results
 */
function outputFWFile($xcifile, $fwfilename)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $xcifile;
    XCIUpdatePartition($path, $fwfilename);
}

/**
 * Download specifc file from inside the rom
 *
 * @param string $romfilename relative path to rom
 * @param string $romfile     filename of rom to get file from
 * @param string $type        type of rom file to download
 * @param int    $fileidx     which file to download
 *
 * @return mixed octet-stream of results
 */
function outputDownloadRomFileContents($romfilename, $romfile, $type, $fileidx)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $romfilename;
    downloadromFileContents($path, $romfile, $type, $fileidx);
}

/**
 * Decompresses NSZ file
 *
 * @param string $compressedpath relative path to rom
 *
 * @return mixed octet-stream of results
 */
function outputdecompressNSZ($compressedpath)
{
    global $gameDir;
    $path = $gameDir . DIRECTORY_SEPARATOR . $compressedpath;
    decompressNSZ($path);
}

/**
 * Scans and renames all rom files returning information via SSE
 *
 * @param bool $confirm rename (true) or just output scan results
 *
 * @return mixed output returned via SSE
 */
function renameAll($confirm = false)
{
    ini_set('max_execution_time', 0);

    global $gameDir, $allowedExtensions;
    $allowedExtRegex = $files = $changeNeeded = $results = array();

    foreach ($allowedExtensions as $ext) {
        array_push($allowedExtRegex, "/$ext\$/");
    }

    if ($confirm != "Confirmed") {
        $scanned = (getFileList($gameDir));
        $files = preg_filter($allowedExtRegex, '$0', $scanned);
    } else {
        $files = json_decode(file_get_contents(CACHE_DIR . "/scanresults.json"));
    }

    /**
     * Creates shutdown function used if there are critical errors caused by rom info timeout
     *
     * @return mixed close SSE handler
     */
    function shutdown()
    {
        $error = error_get_last();
        if ($error !== null && $error['type'] === E_ERROR) {
            echo "data: PROCESSING-ERROR" . PHP_EOL . PHP_EOL;
            flush();
        }
    }
    register_shutdown_function('NSPIndexer\shutdown');

    echo "data: " . count($files) . PHP_EOL . PHP_EOL;
    flush();

    foreach ($files as $file) {
        echo "data: " . $file . PHP_EOL . PHP_EOL;
        flush();

        if ($confirm != "Confirmed") {
            set_time_limit(5);
            $results = json_decode(renameRom($file), true);
            set_time_limit(0);
            if ($results["old"] != $results["new"]) {
                array_push($changeNeeded, $file);
            }
        } else {
            set_time_limit(5);
            renameRom($file, false);
            set_time_limit(0);
        }
    }

    if ($confirm != "Confirmed") {
        file_put_contents(CACHE_DIR . "/scanresults.json", json_encode($changeNeeded));
    } else {
        unlink(CACHE_DIR . "/scanresults.json");
    }

    sleep(1);
    echo "data: RENAME-JOB-COMPLETE" . PHP_EOL . PHP_EOL;
    flush();

    ini_restore('max_execution_time');
}

/**
 * Creates file with skip extension to mute alert
 *
 * @param string $titleId title id to mute
 * @param string $toggle  create or remove skip file
 * @param string $type    update or dlc
 *
 * @return mixed no return data
 */
function muteToggle($titleId, $toggle, $type)
{
    global $gameDir;

    $titlesJson = getMetadata("titles");
    $versionsJson = getMetadata("versions");
    $titleIdType = getTitleIdType($titleId);
    $baseTitleId = getBaseTitleId($titleId);
    $baseTitleName = preg_replace("/[^[:alnum:][:space:]_+-]/u", '', $titlesJson[$baseTitleId]['name']);

    $dlcName = $dlcNiceName = "";
    if ($type == "dlc") {
        $dlcName = preg_replace("/[^[:alnum:][:space:]_+-]/u", '', $titlesJson[$titleId]['name']);
        $dlcNameNice = "(" . trim(str_replace($baseTitleName, '', $dlcName)) . ") ";
        $version = 0;
    } elseif ($type == "update") {
        $version = array_key_last($versionsJson[strtoupper($baseTitleId)]);
    }

    $newName = $baseTitleName . '/' . $baseTitleName . " " . $dlcNameNice . "[" . $titleId . "][v" . $version . "].txt";

    if (!file_exists($gameDir . '/' . $baseTitleName)) {
        mkdir($gameDir . '/' . $baseTitleName);
    }

    if ($toggle == "mute") {
        file_put_contents($gameDir . "/" . $newName, '');
    } elseif ($toggle == "unmute") {
        unlink($gameDir . "/" . $newName);
    }
}

/**
 * Removes superseded update files
 *
 * @param string $path relative path of the old update file
 *
 * @return array success of railure
 */
function oldUpdate($path)
{
    global $gameDir;

    $cleanPath = urldecode($path);

    if (preg_match("/\[v[1-9][0-9]{4,}\].nsp/", $cleanPath)) {
        unlink($gameDir . "/" . $cleanPath);
        return json_encode(array( "int" => 0 ));
    } else {
        return json_encode(array( "int" => 1 ));
    }
}

if (isset($_GET["config"])) {
    header("Content-Type: application/json");
    echo outputConfig();
    die();
} elseif (isset($_GET["titles"])) {
    header("Content-Type: application/json");
    echo outputTitles(isset($_GET["force"]));
    die();
} elseif (isset($_GET['metadata'])) {
    header("Content-Type: application/json");
    echo refreshMetadata();
    die();
} elseif (!empty($_GET['rename'])) {
    header("Content-Type: application/json");
    echo renameRom(rawurldecode($_GET['rename']), isset($_GET['preview']));
    die();
} elseif (!empty($_GET['rominfo'])) {
    header("Content-Type: application/json");
    echo outputRomInfo(rawurldecode($_GET['rominfo']));
    die();
} elseif (!empty($_GET['romfilename'])) {
    header("Content-Type: application/json");
    echo outputRomFile(rawurldecode($_GET['romfilename']), $_GET['romfile']);
    die();
} elseif (!empty($_GET['xcifile'])) {
    header("Content-Type: application/json");
    echo outputFWFile(rawurldecode($_GET['xcifile']), $_GET['fwfilename']);
    die();
} elseif (!empty($_GET['romfilecontents'])) {
    header("Content-Type: application/json");
    echo outputRomFileContents(rawurldecode($_GET['romfilecontents']), $_GET['romfile']);
    die();
} elseif (!empty($_GET['downloadfilecontents'])) {
    header("Content-Type: application/json");
    echo outputDownloadRomFileContents(rawurldecode($_GET['downloadfilecontents']), $_GET['romfile'], $_GET['type'], $_GET['fileidx']);
    die();
} elseif (!empty($_GET['ncafileanalyze'])) {
    header("Content-Type: application/json");
    echo outputncafileAnalyze(rawurldecode($_GET['ncafileanalyze']), $_GET['romfile']);
    die();
} elseif (!empty($_GET['decompressNSZ'])) {
    header("Content-Type: application/json");
    echo outputdecompressNSZ(rawurldecode($_GET['decompressNSZ']));
    die();
} elseif (isset($_GET["RenameAll"])) {
    header('Content-Type: text/event-stream');
    header('Cache-Control: no-cache');
    header('X-Accel-Buffering: no');
    echo renameAll($_GET["RenameAll"]);
    die();
} elseif (isset($_GET["muteToggle"])) {
    muteToggle($_GET['titleId'], $_GET['toggle'], $_GET['type']);
    die();
} elseif (isset($_GET["oldUpdate"])) {
    header("Content-Type: application/json");
    echo oldUpdate($_GET['oldUpdate']);
    die();
}
require 'page.html';
