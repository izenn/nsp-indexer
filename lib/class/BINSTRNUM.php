<?php

/**
 * AESXTSN class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Ugly class to deal with very long binary string integer (just to remove php-gmp deps)
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class BINSTRNUM
{
    public $binstr;

    /**
     * Function that sets up parameters and values used for BINSTRNUM
     *
     * @param array $binstr array of keys
     *
     * @return mixed properties and values for BINSTRNUM
     */
    public function __construct($binstr)
    {
        $this->binstr = $binstr;
    }

    /**
     * Function that gets the reverse string and passes it and its length to multinc
     *
     * @param array $num binary string in reverse
     *
     * @return mixed properties and values for encrypted sectors
     */
    public function mult($num)
    {
        $this->multinc(strlen($this->binstr) - 1, $num);
    }

    /**
     * Function that processes very long binary strings
     *
     * @param array $i   length of the string
     * @param array $num string to process
     * @param array $inc increment
     *
     * @return string single byte string
     */
    private function multinc($i, $num, $inc = 0)
    {
        $newnum = ord($this->binstr[$i]) * $num;
        if ($newnum > 255 && $i != 0) {
            $newnum = $newnum - 256;
            $this->binstr[$i] = chr($newnum + $inc);
            $this->multinc($i - 1, $num, 1);
        } elseif ($newnum > 255 && $i == 0) {
            $newnum = $newnum - 256;
            $this->binstr[$i] = chr($newnum + $inc);
            $this->binstr = chr(1) . $this->binstr;
        } elseif ($i <= 0) {
            $this->binstr[$i] = chr($newnum + $inc);
        } else {
            $this->binstr[$i] = chr($newnum + $inc);
            $this->multinc($i - 1, $num, 0);
        }
    }
}
