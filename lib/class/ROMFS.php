<?php

/**
 * ROMFS class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to get romfs info
 * ROMFS very partial implementation, for use on small files only (we just need this)
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class ROMFS
{
    public $nacp;
    public $Files;
    public $aesctr;
    public $data_offset;
    public $decData;
    public $dir_hash_offset;
    public $dir_hash_size;
    public $dir_meta_offset;
    public $dir_meta_size;
    public $Directorys;
    public $encoffset;
    public $encsize;
    public $fh;
    public $file_hash_offset;
    public $file_hash_size;
    public $file_meta_offset;
    public $file_meta_size;
    public $headerSize;
    public $romfsoffset;
    public $romfssize;
    public $startctr;

    /**
     * Creates properties and values for object
     *
     * @param string $fh          file handler
     * @param string $encOffset   offset of the encrypted portion of the file
     * @param string $encSize     size of the encrypted portion of the file
     * @param string $romfsOffset offset for the romfs partition
     * @param string $romfsSize   size of the romfs partition
     * @param string $key         key to use for decryption
     * @param string $ctr         CTR info
     *
     * @return mixed data file from the romfs partition
     */
    public function __construct($fh, $encOffset, $encSize, $romfsOffset, $romfsSize, $key, $ctr)
    {
        $this->aesctr = new AESCTR(hex2bin(strtoupper($key)), hex2bin(strtoupper($ctr)), true);
        $this->fh = $fh;
        $this->startctr = hex2bin($ctr);
        $this->encoffset = $encOffset;
        $this->encsize = $encSize;
        $this->romfsoffset = $romfsOffset;
        $this->romfssize = $romfsSize;
        fseek($fh, $romfsOffset);
        $this->decData =  $this->aesctr->decrypt(fread($fh, 0x50), $this->getCtrOffset($romfsOffset - $encOffset));
    }

        /**
         * Gets header info about the romfs
         *
         * @return mixed header info
         */
    public function getHeader()
    {
        $this->headerSize = unpack("P", substr($this->decData, 0, 8))[1];
        $this->dir_hash_offset = unpack("P", substr($this->decData, 0x08, 8))[1];
        $this->dir_hash_size = unpack("P", substr($this->decData, 0x10, 8))[1];
        $this->dir_meta_offset = unpack("P", substr($this->decData, 0x18, 8))[1];
        $this->dir_meta_size = unpack("P", substr($this->decData, 0x20, 8))[1];

        $this->file_hash_offset = unpack("P", substr($this->decData, 0x28, 8))[1];
        $this->file_hash_size = unpack("P", substr($this->decData, 0x30, 8))[1];
        $this->file_meta_offset = unpack("P", substr($this->decData, 0x38, 8))[1];
        $this->file_meta_size = unpack("P", substr($this->decData, 0x40, 8))[1];
        $this->data_offset = unpack("P", substr($this->decData, 0x48, 8))[1];

        $this->Files = array();
        $this->Directorys = array();
        $lenred = 0;
        $idx = 0;
        $this->parseDir(0);

        for ($i = 0; $i < count($this->Files); $i++) {
            $parts = explode('.', $this->Files[$i]["name"]);
            if ($parts[count($parts) - 1] == "nacp") {
                $this->nacp = new NACP($this->getFile($i));
            }
        }

        for ($i = 0; $i < count($this->Files); $i++) {
            if (substr($this->Files[$i]["name"], 0, 6) == "/icon_") {
                $fileidx = $this->nacp->getLangIdx($this->Files[$i]["name"]);
                $this->nacp->langs[$fileidx]->gameIcon = base64_encode($this->getFile($i));
                $this->nacp->langs[$fileidx]->iconFilename = $this->Files[$i]["name"];
            }
        }
    }

    /**
     * Parses an internal file to get the file info
     *
     * @param string $off  offset
     * @param string $path path to the file being parsed
     *
     * @return mixed header info
     */
    private function parseFile($off, $path = '')
    {
        $filearray = array();
        $filearray["sibling"] = 0;
        while ($filearray["sibling"] != 0xFFFFFFFF) {
            $subber = ($this->file_meta_offset + $off) % 16;
            fseek($this->fh, $this->romfsoffset + $this->file_meta_offset + $off - $subber);
            $mydata =  $this->aesctr->decrypt(fread($this->fh, 0x20 + $subber), $this->getCtrOffset($this->romfsoffset - $this->encoffset + $this->file_meta_offset + $off - $subber));
            $tmpfdata = substr($mydata, 0 + $subber, 0x20);
            $filearray = unpack("Vparent/Vsibling/Pofs/Psize/Vhsh/Vnamelen", $tmpfdata);

            $subber = ($this->file_meta_offset + $off + 0x20) % 16;
            fseek($this->fh, $this->romfsoffset + $this->file_meta_offset + $off + 0x20 - $subber);
            $tmpfilename = $this->aesctr->decrypt(fread($this->fh, $filearray['namelen'] + $subber), $this->getCtrOffset($this->romfsoffset - $this->encoffset + $this->file_meta_offset + $off + 0x20 - $subber));
            $tmpfilename = substr($tmpfilename, 0 + $subber, $filearray['namelen']);
            $filearray["name"] = $path . $tmpfilename;
            $off = $filearray["sibling"];
            $this->Files[] = $filearray;
        }
    }

    /**
     * Parses an internal directory to get the file contents
     *
     * @param string $off  offset
     * @param string $path path to the directory being parsed
     *
     * @return mixed header info
     */
    private function parseDir($off, $path = '')
    {
        $dirsubber = ($this->dir_meta_offset + $off) % 16;
        fseek($this->fh, $this->romfsoffset + $this->dir_meta_offset + $off - $dirsubber);
        $mydata = $this->aesctr->decrypt(fread($this->fh, 0x18 + $dirsubber), $this->getCtrOffset($this->romfsoffset - $this->encoffset + $this->dir_meta_offset + $off - $dirsubber));
        $mydata = substr($mydata, 0 + $dirsubber, 0x18);
        $dirarray = unpack("Vparent/Vsibling/Vchild/Vfile/Vhsh/Vnamelen", $mydata);
        if ($dirarray["namelen"] != 0xFFFFFFFF) {
            $dirsubber = ($this->dir_meta_offset + $off + 0x18) % 16;
            fseek($this->fh, $this->romfsoffset + $this->dir_meta_offset + $off + 0x18 - $dirsubber);
            $tmpdirname = $this->aesctr->decrypt(fread($this->fh, $dirarray['namelen'] + $dirsubber), $this->getCtrOffset($this->romfsoffset - $this->encoffset + $this->dir_meta_offset + $off + 0x18 - $dirsubber));
            $tmpdirname = substr($tmpdirname, 0 + $dirsubber, $dirarray['namelen']);
            $dirarray["name"] = $tmpdirname;
        } else {
            $dirarray["name"] = "";
        }
        $newpath = "";
        if ($path != '') {
             $newpath = $path . $dirarray["name"] . "/";
        } else {
            $newpath = $dirarray["name"] . "/";
        }

        if ($dirarray["file"] != 0xFFFFFFFF) {
            $this->parseFile($dirarray["file"], $newpath);
        }

        if ($dirarray["sibling"] != 0xFFFFFFFF) {
            $this->parseDir($dirarray["sibling"], $path);
        }
        if ($dirarray["child"] != 0xFFFFFFFF) {
            $this->parseDir($dirarray["child"], $newpath);
        }
    }

    /**
     * Gets CTR offset
     *
     * @param string $offset offset
     *
     * @return mixed header info
     */
    public function getCtrOffset($offset)
    {
        $ctr = new CTRCOUNTERGMP($this->startctr);
        $adder = $offset / 16;
        $ctr->add($adder);
        return $ctr->getCtr();
    }

        /**
         * In memory extraction use on small file only
         *
         * @param $idx index of the file to download
         *
         * @return mixed data file
         */
    public function getFile($idx)
    {
        fseek($this->fh, $this->romfsoffset + $this->data_offset + $this->Files[$idx]["ofs"]);
        $filecontents = $this->aesctr->decrypt(fread($this->fh, $this->Files[$idx]["size"]), $this->getCtrOffset(($this->romfsoffset - $this->encoffset) + $this->data_offset + $this->Files[$idx]["ofs"]));
        return $filecontents;
    }

    /**
     * File extraction for larger files
     *
     * @param $idx index of the file to download
     *
     * @return mixed data file
     */
    public function extractFile($idx)
    {

        $subber = ($this->romfsoffset + $this->data_offset + $this->Files[$idx]["ofs"]) % 16;

        fseek($this->fh, $this->romfsoffset + $this->data_offset + $this->Files[$idx]["ofs"] - $subber);

        $size = $this->Files[$idx]["size"];
        $tmpchunksize = $size + $subber;
        $parts = explode('/', $this->Files[$idx]["name"]);
        $chunksize = 5 * (1024 * 1024);
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $size);
        header('Content-Disposition: attachment;filename="' . $parts[count($parts) - 1] . '"');
        $tmpchunksize = $size + $subber;
        $tmpchunkdone = 0;
        while ($tmpchunksize > $chunksize) {
            $ctr = $this->getCtrOffset(($this->romfsoffset - $this->encoffset) + $this->data_offset + $this->Files[$idx]["ofs"] - $subber + ($chunksize * $tmpchunkdone));
            $outdata =  $this->aesctr->decrypt(fread($this->fh, $chunksize), $ctr);
            if ($tmpchunkdone == 0) {
                print(substr($outdata, $subber));
            } else {
                print($outdata);
            }
            $tmpchunksize -= $chunksize;
            $tmpchunkdone += 1;

            flush();
        }

        if ($tmpchunksize <= $chunksize) {
            $ctr = $this->getCtrOffset(($this->romfsoffset - $this->encoffset) + $this->data_offset + $this->Files[$idx]["ofs"] - $subber + ($chunksize * $tmpchunkdone));
            $outdata = $this->aesctr->decrypt(fread($this->fh, $tmpchunksize), $ctr);
            if ($tmpchunkdone == 0) {
                print(substr($outdata, $subber));
            } else {
                print($outdata);
            }
            flush();
        }
    }
}
